
const УКАЗАТЕЛЬ = "https://bitbucket.org/gitjs/ochistit-clear/raw/master/0000";


// // // //


ОчиститьПусковойСкрипт = function(мир)
{
    localStorage.removeItem("0000");
    document.body.innerHTML += `<p>Очистили пусковой скрипт | Removed startup script</p>`;
    мир.уведомить("очистили пусковой скрипт");
};


// // // //


ОчиститьЭтотМодуль = function(мир)
{
    localforage.removeItem(УКАЗАТЕЛЬ, function(ошибка, значение) {
        if (!ошибка)
        {
            document.body.innerHTML += `<p>Очистили этот модуль | Removed this module</p>`;
            мир.уведомить("очистили этот модуль");
        }
        else
        {
            document.body.innerHTML += `<p>ОШИБКА Не удалось очистить этот модуль | ERROR Could not remove this module: '${ошибка}'</p>`;
        }
    });
};


// // // //


ОчиститьВсеМодули = function(мир)
{
    localforage.clear(function(ошибка, значение) {
        if (!ошибка)
        {
            document.body.innerHTML += `<p>Очистили все модули | Removed all modules</p>`;
            document.body.innerHTML += `<strong>ГОТОВО | DONE</strong>`;
        }
        else
        {
            document.body.innerHTML += `<p>ОШИБКА Не удалось очистить все модули | ERROR Could not remove all modules: '${ошибка}'</p>`;
        }
    });
};


